;;; -*- lexical-binding: t -*-

(dolist (pkg '("~/.emacs.d/file-modes/dockerfile-mode"
               "~/.emacs.d/file-modes/editorconfig"
               "~/.emacs.d/file-modes/git-modes"
               "~/.emacs.d/file-modes/ledger-mode"
               "~/.emacs.d/file-modes/markdown-mode"
               "~/.emacs.d/file-modes/pkgbuild-mode"
               "~/.emacs.d/file-modes/ssh-file-modes"
               "~/.emacs.d/file-modes/todotxt"
               "~/.emacs.d/file-modes/yaml-mode"))
  (add-to-list 'load-path pkg))

;;;;;;;;;;;;;;;;;;;;
;; Built-in modes ;;
;;;;;;;;;;;;;;;;;;;;

;; language servers
(require 'eglot)
(add-hook 'js-mode-hook #'eglot-ensure)
(add-hook 'python-mode-hook #'eglot-ensure)

;; documentation
(require 'eldoc)
(setq eldoc-echo-area-use-multiline-p nil)
(setq eldoc-echo-area-prefer-doc-buffer t)
(global-set-key (kbd "C-h .") 'eldoc-print-current-symbol-info)

;; syntax checking
(require 'flymake)
(global-set-key (kbd "M-g n") #'flymake-goto-next-error)
(global-set-key (kbd "M-g M-n") #'flymake-goto-next-error)
(global-set-key (kbd "M-g p") #'flymake-goto-prev-error)
(global-set-key (kbd "M-g M-p") #'flymake-goto-prev-error)
(global-set-key (kbd "M-g l") #'flymake-show-buffer-diagnostics)
(global-set-key (kbd "M-g M-l") #'flymake-show-buffer-diagnostics)

(require 'cc-mode)
(setq c-auto-align-backslashes nil)
(setq c-default-style '((java-mode . "java")
                        (awk-mode . "awk")
                        (other . "stroustrup")))

(require 'perl-mode)
(setq perl-indent-level 4)
(setq perl-continued-statement-offset 0)
(setq perl-continued-brace-offset 0)
(setq perl-brace-offset 0)
(setq perl-brace-imaginary-offset 0)
(setq perl-label-offset 0)
(setq perl-indent-continued-arguments 4)

(require 'python)
(add-hook 'python-mode-hook
          (lambda ()
            (setq forward-sexp-function nil)
            (set (make-local-variable 'python-indent-offset) 4)))

(require 'sh-script)
(setq sh-use-smie nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Third-party packages ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'dockerfile-mode)

(require 'editorconfig)
(setq editorconfig-mode-lighter "")
(editorconfig-mode 1)

(require 'git-modes)

(when (file-exists-p "~/.emacs.d/accounts.ledger")
  (setq ledger-accounts-file "~/.emacs.d/accounts.ledger")
  (require 'ledger-mode nil 'noerror)
  (add-to-list 'auto-mode-alist '("\\.ledger\\'" . ledger-mode))
  (setq ledger-post-amount-alignment-column 72)
  (setq ledger-reconcile-default-commodity "AUD")
  (setq ledger-clear-whole-transactions t)
  (setq org-read-date-prefer-future nil)
  (add-hook 'ledger-mode-hook
            (lambda ()
	      (setq-local vertico-sort-function #'vertico-sort-alpha)))
  (custom-set-faces
   '(ledger-font-posting-account-face ((t (:inherit default))))))

(require 'markdown-mode)
(setq markdown-command "markdown_py -x markdown.extensions.smart_strong -x markdown.extensions.fenced_code -x markdown.extensions.nl2br")
(add-to-list 'auto-mode-alist '("\\.mdown\\'" . markdown-mode) t)

(require 'pkgbuild-mode)

(require 'ssh-file-modes)

(require 'todotxt)
(setq todotxt-file "~/Todo/todo.txt")
(add-to-list 'auto-mode-alist `(,(expand-file-name "~/Todo/todo.txt") . todotxt-mode))

(require 'yaml-mode)
