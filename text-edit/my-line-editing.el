;;; -*- lexical-binding: t -*-

(defun my/copy-line (arg)
  "Copy the current line into the kill ring. With ARG copies that many lines."
  (interactive "p")
  (kill-ring-save (line-beginning-position 1)
                  (line-beginning-position (+ 1 arg)))
  (message "Copied %d lines" arg))

(defun my/open-line-above (arg)
  "Open a new line above point with indentation. With ARG insert that many lines."
  (interactive "*p")
  (beginning-of-line)
  (newline arg)
  (forward-line (- arg))
  (indent-for-tab-command))

(defun my/open-line-below (arg)
  "Open a new line below point with indentation. With ARG insert that many lines."
  (interactive "*p")
  (end-of-line)
  (newline arg)
  (indent-for-tab-command))

(defun my/substitute-line (arg)
  "Kill the current line and leave point at correct indentation level. With ARG kill that many lines first."
  (interactive "*P")
  (beginning-of-line)
  (if (not (and (null arg) (equal (line-beginning-position) (line-end-position))))
      (kill-line arg))
  (if (not (string-equal major-mode "fundamental-mode"))
      (indent-for-tab-command)))

(provide 'my-line-editing)
