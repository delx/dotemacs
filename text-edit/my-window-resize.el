;;; -*- lexical-binding: t -*-

(defun my/window-resize-mode ()
  (interactive)
  (message "Resize window with the arrow keys")
  (let ((keymap (make-sparse-keymap)))
    (define-key keymap [up] #'enlarge-window)
    (define-key keymap [down] #'shrink-window)
    (define-key keymap [left] #'shrink-window-horizontally)
    (define-key keymap [right] #'enlarge-window-horizontally)
    (set-transient-map keymap t)))

(provide 'my-window-resize)
