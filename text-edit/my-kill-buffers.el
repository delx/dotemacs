;;; -*- lexical-binding: t -*-

(defun my/kill-buffers-if-deleting-last-frame (frame)
  "Kill all buffers when closing the last frame"
  (when (equal (list frame) (my/frame-list-ignoring-initial-frame))
    (dolist (buffer (buffer-list))
      (my/kill-buffer-safely buffer))))

(defun my/kill-buffers-not-in-frame ()
  "Kill buffers which are not loaded into some frame"
  (interactive)
  (let ((kill-count 0))
    (dolist (buffer (buffer-list))
      (let* ((window (get-buffer-window buffer t))
             (frame (window-frame window)))
        (if (or (null frame) (not (window-live-p window)) (my/frame-initial-frame-p frame))
            (if (my/kill-buffer-safely buffer)
                (cl-incf kill-count)))))
    (message "Killed %d buffers" kill-count)))



(defun my/frame-initial-frame-p (frame)
  "Returns true if the given frame is the magic 'initial frame' that always exists in GUI emacs sessions"
  (equal "initial_terminal" (terminal-name frame)))

(defun my/frame-list-ignoring-initial-frame ()
  "Returns all frames except the magic 'initial frame' that always exists in GUI emacs sessions"
  (filtered-frame-list (lambda (frame) (not (my/frame-initial-frame-p frame)))))

(defun my/kill-buffer-safely (buffer)
  "Kill the buffer if it is not special or modified"
  (if (and
       (not (string-match "^ " (buffer-name buffer)))
       (not (equal "*Messages*" (buffer-name buffer)))
       (or
        (not (buffer-modified-p buffer))
        (null (buffer-file-name buffer))))
      (kill-buffer buffer)))

(provide 'my-kill-buffers)
