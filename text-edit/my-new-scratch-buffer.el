;;; -*- lexical-binding: t -*-

(setq initial-scratch-message nil)
(setq initial-major-mode 'emacs-lisp-mode)

(defun my/new-scratch-buffer ()
  "Create a new scratch buffer to work in. (could be *scratch* - *scratchX*)"
  (interactive)
  (let ((n 0)
        bufname)
    (while (progn
             (setq bufname (concat "*scratch"
                                   (if (= n 0) "" (int-to-string n))
                                   "*"))
             (setq n (1+ n))
             (get-buffer bufname)))
    (switch-to-buffer (get-buffer-create bufname))
    (funcall initial-major-mode)))

(provide 'my-new-scratch-buffer)
