;;; -*- lexical-binding: t -*-

;; Auto-cleanup and auto-save with no (message)

(defvar my/recentf-list-prev nil)

(defun my/recentf-periodic ()
  (when (not (equal recentf-list my/recentf-list-prev))
    (let ((inhibit-message t)) (recentf-cleanup))
    (let ((inhibit-message t)) (recentf-save-list))
    (setq my/recentf-list-prev recentf-list)))

(defun my/recentf-auto-cleanup-and-save ()
  (run-with-idle-timer 30 t #'my/recentf-periodic))

(provide 'my-recentf)
