;;; -*- lexical-binding: t -*-

(dolist (pkg '("~/.emacs.d/text-edit"
               "~/.emacs.d/text-edit/change-inner"
               "~/.emacs.d/text-edit/expand-region"
               "~/.emacs.d/text-edit/highlight-symbol"
               "~/.emacs.d/text-edit/jump-char"
               "~/.emacs.d/text-edit/yasnippet"))
  (add-to-list 'load-path pkg))

;;;;;;;;;;;;;;;;;
;; My packages ;;
;;;;;;;;;;;;;;;;;

(require 'my-buffer-navigation)
(global-set-key (kbd "C-x b") 'my/switch-to-previous-buffer)

(require 'my-find-test-file)
(global-set-key (kbd "C-x t") 'my/find-prod-or-test-file)

(require 'my-frame-title)
(setq frame-title-format '((:eval (funcall #'my/generate-frame-title))))
(setq icon-title-format frame-title-format)
(add-to-list 'post-command-hook #'my/terminal-update-title)

(require 'my-kill-buffers)
(add-to-list 'delete-frame-functions #'my/kill-buffers-if-deleting-last-frame)
(global-set-key (kbd "C-x c") 'my/kill-buffers-not-in-frame)

(require 'my-line-editing)
(global-set-key (kbd "C-o") 'my/open-line-above)
(global-set-key (kbd "M-o") 'my/open-line-below)
(global-set-key (kbd "C-c c") 'my/copy-line)
(global-set-key (kbd "C-c s") 'my/substitute-line)

(require 'my-new-scratch-buffer)

(require 'my-recentf)
(setq recentf-save-file "~/.cache/emacs/recentf")
(setq recentf-exclude '("/\\.emacs\\.d/recentf$" "/\\.git/"))
(setq recentf-max-saved-items 1000)
(recentf-mode 1)
(my/recentf-auto-cleanup-and-save)

(require 'my-window-resize)
(global-set-key (kbd "C-x 9") 'my/window-resize-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Third-party packages ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'change-inner)
(global-set-key (kbd "M-i") 'change-inner)

(require 'expand-region)
(global-set-key (kbd "M-=") 'er/expand-region)

(require 'highlight-symbol)
(add-hook 'prog-mode-hook #'highlight-symbol-mode)
(add-hook 'prog-mode-hook #'highlight-symbol-nav-mode)
(setq highlight-symbol-idle-delay 0.5)

(require 'jump-char)
(global-set-key (kbd "M-g f") 'jump-char-forward)

(require 'yasnippet)
(add-hook 'prog-mode-hook #'yas-minor-mode)
(add-hook 'python-mode-hook
          (lambda () (set (make-local-variable 'yas-indent-line) 'fixed)))
(yas-reload-all)
