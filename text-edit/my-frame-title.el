;;; -*- lexical-binding: t -*-

(defun my/generate-frame-title ()
  "Return a string like \"filename (dirname) (hostname)\"."
  (let ((t-name
         (if (buffer-file-name)
             (file-name-nondirectory (buffer-file-name))
           (buffer-name)))
        (t-directory
         (if (buffer-file-name)
             (concat
              " ("
              (abbreviate-file-name
               (substring (file-name-directory (buffer-file-name)) 0 -1))
              ")")))
        (t-modified
         (if (buffer-modified-p)
             " +")))
    (concat
     t-name
     t-modified
     t-directory
     " (" (system-name) ")")))

(defun my/terminal-update-title ()
  "If using a terminal frame then sends the escape codes to update the title."
  (if (terminal-parameter (frame-terminal) 'terminal-initted)
      (send-string-to-terminal (concat "\033]0;" (my/generate-frame-title) "\007"))))

(provide 'my-frame-title)
