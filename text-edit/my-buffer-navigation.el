;;; -*- lexical-binding: t -*-

(defun my/switch-to-previous-buffer ()
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))

(provide 'my-buffer-navigation)
