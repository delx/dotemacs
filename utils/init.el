;;; -*- lexical-binding: t -*-

(dolist (pkg '("~/.emacs.d/utils/compat/"
               "~/.emacs.d/utils/dash/"
               "~/.emacs.d/utils/s/"))
  (add-to-list 'load-path pkg))
