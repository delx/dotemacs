;;; -*- lexical-binding: t -*-

;;;;;;;;;;;;;;;;
;; Early init ;;
;;;;;;;;;;;;;;;;

(let ((dir "~/.cache/emacs/eln-cache/"))
  (unless (file-accessible-directory-p dir)
    (make-directory dir))
  (startup-redirect-eln-cache dir))

(let ((dir "~/.cache/emacs/saves/"))
  (unless (file-accessible-directory-p dir)
    (make-directory dir))
  (setq auto-save-list-file-prefix dir))

(let ((dir "~/.cache/emacs/backups/"))
  (unless (file-accessible-directory-p dir)
    (make-directory dir))
  (setq backup-directory-alist `(("." . ,dir))))

(setq save-place-file "~/.cache/emacs/places")

(setq project-list-file "~/.cache/emacs/projects")

(setq tramp-persistency-file-name "~/.cache/emacs/tramp")

;;;;;;;;;;;;;;;;;;;;
;; Visual options ;;
;;;;;;;;;;;;;;;;;;;;

(load-file "~/.emacs.d/pastels-on-dark-theme.el")
(enable-theme 'pastels-on-dark)

(setq default-frame-alist '((vertical-scroll-bars . nil)
                            (menu-bar-lines . 0)
                            (tool-bar-lines . 0)
                            (background-mode . dark)
                            (alpha . 95)
                            (font . "monospace 12")))

(column-number-mode t)

(line-number-mode t)

(show-paren-mode t)

(setq-default show-trailing-whitespace t)

;; 256-color mode in screen terminals
(add-to-list 'term-file-aliases
                 '("screen.xterm-256color" . "screen-256color"))

;;;;;;;;;;;;;;;;;;;;;
;; General options ;;
;;;;;;;;;;;;;;;;;;;;;

(add-hook 'text-mode-hook #'goto-address-mode)
(add-hook 'prog-mode-hook #'goto-address-prog-mode)

(global-auto-revert-mode 1)
(setq global-auto-revert-non-file-buffers t)
(setq auto-revert-verbose nil)

(setq auto-save-interval 0)
(setq auto-save-timeout 30)

;; backup files - keep 10 numbered backups
(setq backup-by-copying t)
(setq delete-old-versions t)
(setq kept-new-versions 10)
(setq kept-old-versions 0)
(setq vc-make-backup-files t)
(setq version-control t)

(setq create-lockfiles nil)

(delete-selection-mode t)

(setq disabled-command-function nil)

(setq ediff-window-setup-function 'ediff-setup-windows-plain
      ediff-split-window-function 'split-window-horizontally)

(setq find-file-visit-truename t)

(setq help-window-select t)

(global-hl-line-mode 1)

(setq-default indent-tabs-mode nil)

(setq mouse-1-click-follows-link nil)

(setq mouse-wheel-scroll-amount '(3))
(setq mouse-wheel-progressive-speed nil)

(repeat-mode 1)

(setq-default require-final-newline t)

(setq save-interprogram-paste-before-kill t)

(save-place-mode 1)

(setq scroll-conservatively 1)
(setq scroll-margin 1)

(setq shift-select-mode nil)

(setq sentence-end-double-space nil)

(setq undo-limit 10000000)
(setq undo-strong-limit undo-limit)

;; unique buffer names
(setq uniquify-strip-common-suffix t)
(setq uniquify-buffer-name-style 'post-forward)

(setq vc-follow-symlinks t)

(xterm-mouse-mode 1)

(defalias 'yes-or-no-p 'y-or-n-p)

;;;;;;;;;;;;;;;;;
;; Keybindings ;;
;;;;;;;;;;;;;;;;;

(global-set-key (kbd "C-x f") 'project-find-file)

;; Text formatting
(global-set-key (kbd "M-;") 'comment-line)
(global-set-key (kbd "C-c d") 'duplicate-dwim)
(global-set-key (kbd "M-z") 'zap-up-to-char)
(global-set-key (kbd "C-c k") 'kill-whole-line)

;; Windows/frames
(global-set-key (kbd "<C-up>") 'windmove-up)
(global-set-key (kbd "<C-down>") 'windmove-down)
(global-set-key (kbd "<C-left>") 'windmove-left)
(global-set-key (kbd "<C-right>") 'windmove-right)

;; Mode switching
(global-set-key (kbd "<f7>") 'flymake-mode)
(global-set-key (kbd "<f8>") 'flyspell-mode)
(global-set-key (kbd "<f9>") 'whitespace-mode)
(global-set-key (kbd "<f10>") 'toggle-truncate-lines)

;; Disable some default bindings from the builtin mouse.el
(global-set-key [C-down-mouse-1] nil)
(global-set-key [C-down-mouse-2] nil)
(global-set-key [C-down-mouse-3] nil)
(global-set-key [S-down-mouse-1] nil)
(global-set-key [S-down-mouse-2] nil)
(global-set-key [S-down-mouse-3] nil)

;; Disable suspend-frame, it's annoying
(global-set-key (kbd "C-z") nil)
(global-set-key (kbd "C-x C-z") nil)

;;;;;;;;;;;;;;;;;;;;;;;;
;; Find SSH_AUTH_SOCK ;;
;;;;;;;;;;;;;;;;;;;;;;;;

(dolist (p '("keyring/ssh" "gcr/ssh" "openssh_agent"))
  (let ((SSH_AUTH_SOCK (substitute-env-vars (concat "$XDG_RUNTIME_DIR/" p))))
    (when (file-exists-p SSH_AUTH_SOCK)
      (message "Found SSH_AUTH_SOCK %s" SSH_AUTH_SOCK)
      (setenv "SSH_AUTH_SOCK" SSH_AUTH_SOCK))))

;;;;;;;;;;;;;;;;;;;
;; Load packages ;;
;;;;;;;;;;;;;;;;;;;

(defvar my/original-load-path load-path)

(defun my/reset-load-path ()
  (setq load-path my/original-load-path))

(message "Loading packages...")
(load-file "~/.emacs.d/utils/init.el")
(load-file "~/.emacs.d/completion/init.el")
(load-file "~/.emacs.d/text-edit/init.el")
(load-file "~/.emacs.d/file-modes/init.el")
(load-file "~/.emacs.d/vc/init.el")
(message "Finished loading init.el")
