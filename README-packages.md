# Initial clone
git submodule init
git submodule update

# Adding
git submodule add --name name https://example.com/foo.git .emacs.d/packages/name

# Moving
git mv

# Removing
git submodule deinit
git rm
rm -rf .git/modules/<name>

# Updating
git submodule foreach git fetch --prune
git submodule foreach git diff origin/master --stat|cat
