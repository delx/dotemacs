#!/bin/bash

cd "$(dirname "$0")"

function get_submodules {
    awk -F'"' '/submodule/ {print $2}' | sort
}

echo '.gitmodules -> .git/config'
diff -u <(get_submodules < .gitmodules) <(get_submodules < .git/config)

echo '.gitmodules -> .git/modules'
diff -u <(get_submodules < .gitmodules) <(ls .git/modules)
