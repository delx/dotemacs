# dotemacs

Configuration for Emacs.

## Usage

Initialise the emacs package submodules
```
cd ~/.emacs.d
git submodule init
git submodule update
```

Set up Emacs daemon to autostart:
```
systemctl --user enable --now emacs
```
