# References
* [Reference Card](https://www.gnu.org/software/emacs/refcards/pdf/refcard.pdf)
* [Key Binding Conventions](https://www.gnu.org/software/emacs/manual/html_node/elisp/Key-Binding-Conventions.html)

# General commands
```
C-x C-c                                         Quit Emacs
C-u C-x C-c                                     Save and quit
C-g                                             Cancel command
C-/                                             Undo
C-g C-/                                         Undo undo
C-x z                                           Repeat last command
M-x                                             Execute command
M-:                                             Evaluate expression as elisp
M-x eval-buffer                                 Load current buffer as elisp
C-x C-0                                         Reset font to standard
C-x C-+/=                                       Increase font size
C-x C--                                         Decrease font size
```

# Help
```
C-h .                                           Help at point (Eldoc)
C-h C-h                                         Help
C-h k                                           Help about key binding
C-h v                                           Describe variable
C-h f                                           Describe function
C-h m                                           Documentation on active modes
<prefix> C-h                                    Show bindings starting with <prefix>
C-c C-b                                         Back to previous help topic (while in help-mode)
q                                               Quit window and bury buffer
```

# Search
```
C-s                                             Search forward (consult-line)
C-r                                             Resume vertico search
M-.                                             Find thing at point
M-,                                             Pop tag mark
M-?                                             Find references at point
```

# Files/buffers
```
C-x C-s                                         Save current buffer
C-x s                                           Save some buffers
C-x C-w                                         Write buffer to file
C-x C-f                                         Find file
C-x f                                           Find files in project
C-x g                                           Grep in project
C-x k                                           Kill buffer
C-x c                                           Clean up all buffers that are not visible
C-x b                                           Switch to previous buffer
C-x t                                           Switch between matching prod/test file
C-x C-b                                         Show buffers
C-x C-r                                         Show recent files
M-x scratch-buffer                              Create new scratch buffer
```

# Windows/frames
```
C-x o                                           Switch to other window
C-x 0                                           Delete window
C-x 1                                           Switch to single window
C-x 2                                           Split vertically
C-x 3                                           Split horizontally
C-x 9                                           Resize windows with arrow keys
C-x { / C-x }                                   Shrink/enlarge window horizontally
C-Up / C-Down / C-Left / C-Right                Switch windows
C-l                                             Recenter display (center, top then bottom)
C-M-l                                           Reposition window to contain useful text (eg entire defun)
C-x 5 2                                         New frame (X11 window)
```

# Movement
```
M-m                                             Jump back to start of indentation
M-r                                             Jump to window top/bottom/middle
M-g Tab                                         Jump to column
M-g c                                           Jump to character location
M-g f                                           Jump to character, ;/, to go forward/back
M-g i                                           Show imenu overview of file
M-g g                                           Jump to visible line or any line
M-g p                                           Previous error
M-g n                                           Next error
M-g l                                           Show all errors
C-a                                             Beginning of line
C-e                                             End of line
C-p                                             Back one line
C-n                                             Forward one line
C-b                                             Back one character
C-f                                             Forward one character
M-b                                             Back a word
M-f                                             Forward a word
M-a                                             Backward sentence
M-e                                             Forward sentence
M-v                                             Back page
C-v                                             Forward page
M-{                                             Back one paragraph
M-}                                             Forward one paragraph
M-<                                             Top of file
M->                                             End of file
C-M-p                                           Back to start of balanced expression
C-M-n                                           Forward to end of balanced expression
C-M-b                                           Back one s-expression / symbol
C-M-f                                           Forward one s-expression / symbol
C-M-u                                           Move up one level of parentheses
```

# Text formatting
```
Tab                                             Indent current line correctly
C-x Tab                                         Indent region with left/right
M-/                                             Expand at point
C-M-i                                           Complete at point
M-Tab                                           Auto-complete at point
M-;                                             Comment line or region
M-i                                             Change inner region
M-q                                             Reformat paragraph
M-l                                             Lower case word
M-u                                             Upper case word
M-c                                             Capitalise word
C-x C-l                                         Lower case region
C-x C-u                                         Upper case region
C-o                                             Insert a new line above point
M-o                                             Insert a new line below point
C-j                                             Newline and maybe indent
M-^                                             Join to previous line
M-j                                             New line with comment
C-q                                             Quoted insert
C-M-\                                           Indent region
C-c c                                           Copy line
C-c d                                           Duplicate line
M-x comment-box                                 Wrap region in comment box
```

# Marking
```
C-Space                                         Set mark (start selection)
C-Space C-Space                                 Set mark to point
C-u C-space                                     Pop mark
C-x C-space                                     Pop global mark
C-x C-x                                         Exchange point and mark
C-M-h                                           Highlight expression
M-@                                             Set mark at end of next word
M-=                                             Expand region
```


# Highlighting
```
M-s h u                                         Unhighlight regexp
M-s h .                                         Highlight symbol at point
M-s h p                                         Highlight phrase
M-s h r                                         Highlight regexp
M-s h l                                         Highlight lines matching regexp
```

# Killing and deleting
```
C-M-k                                           Kill s-expression / symbol
C-w                                             Kill from mark to point
C-y                                             Yank text (paste)
M-y                                             Scroll through kill ring (paste buffer)
M-z <char>                                      Kill until <char>
C-k                                             Kill to end of line
M-0 C-k                                         Kill to beginning of line
C-c k                                           Kill whole line
C-c s                                           Replace line
C-d                                             Delete next character
M-d                                             Delete next word
M-Backspace                                     Delete previous word
M-\                                             Delete spaces around point
C-x C-o                                         Collapse blank lines
M-x delete-trailing-whitespace                  Delete trailing whitespace
```

# Transposing
```
C-t                                             Transpose chars
M-t                                             Transpose words
C-x C-t                                         Transpose lines
C-M-t                                           Transpose s-expressions
```

# Rectangle
```
C-x Space                                       Toggle mark rectangle mode
C-x r t                                         Replace rectangle with string
```

# Macros
```
F3 , F4                                         Start recording macro, finish recording macro
C-u F3                                          Play last macro then append keys
F4                                              Play back macro
C-x C-k b                                       Bind macro to key (0-9, A-Z)
C-x C-k C-e                                     Edit last macro
C-x C-k l                                       Edit recent keys as macro
C-x C-k r                                       Apply macro to region lines
```

# Registers
```
C-x r s <?>                                     Copy region into register
C-x r i <?>                                     Insert text from register
C-x r Space <?>                                 Save point position to register
C-x r j <?>                                     Jump to point position at register
```

# Mouse secondary selection
```
Meta-Drag                                       Set selection
Meta-Right-Click                                Yank secondary
```


# Mode switching
```
F7                                              Toggle flymake-mode
F8                                              Toggle flyspell-mode
F9                                              Toggle whitespace mode
F10                                             Toggle word wrap
M-s o                                           Search in buffer and display results in occur-mode
M-x multi-occur                                 Search in buffers and display results in occur-mode
M-x eshell                                      Run eshell
M-x overwrite-mode                              Switch to overwrite mode
M-x shell                                       Run bash shell
M-x smerge-ediff                                Resolve merge conflicts in multi-window mode
M-x smerge-mode                                 Resolve merge conflicts by hunks
M-x term                                        Run terminal
M-x yas-reload-all                              Reload all snippets
```

# Occur
```
g                                               Reload
e                                               Switch to edit mode
C-c C-c                                         Switch back to occur mode
```

# Magit / diff-hl
```
C-x v *                                         Popup diff
C-x v S                                         Stage hunk
C-x v [                                         Previous hunk
C-x v ]                                         Next hunk
C-x v n                                         Revert hunk
C-x v s                                         Magit status
C-x v f                                         Magit file specific commands
C-x v g                                         Magit dispatch popup
p                                               Previous section
n                                               Next section
M-p                                             Previous sibling section
M-n                                             Next sibling section
^                                               Up section
Tab                                             Toggle visibility of section
C-Tab                                           Cycle visibility of section and its children
q                                               Kill window and bury buffer
C-c C-c                                         Perform action
C-c C-k                                         Cancel action
```

# Lisp
```
C-M-x                                           Eval top-level form
C-c C-z                                         Switch to REPL
C-c C-c                                         Compile and load the top-level form at point
C-c M-k                                         Compile (but not load) the current buffer's file
```

# Shell
```
M-!                                             Execute command
M-&                                             Execute command asynchronously
M-|                                             Execute with region
C-u M-|                                         Filter with region
```

# Smerge mode
```
C-c ^ p                                         Go to previous hunk
C-c ^ n                                         Go to next hunk
C-c ^ Return                                    Keep current
C-c ^ a                                         Keep all
C-c ^ m                                         Keep mine
C-c ^ o                                         Keep other
```

# Flyspell
```
M-$                                             Correct word
C-;                                             Automatically correct last word
<mouse-2>                                       Popup word correction
```
