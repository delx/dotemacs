;;; -*- lexical-binding: t -*-

(dolist (pkg '("~/.emacs.d/completion/vertico"
               "~/.emacs.d/completion/vertico/extensions"
               "~/.emacs.d/completion/orderless"
               "~/.emacs.d/completion/marginalia"
               "~/.emacs.d/completion/consult"))
  (add-to-list 'load-path pkg))

(require 'vertico)
(require 'vertico-repeat)
(vertico-mode 1)
(add-hook 'minibuffer-setup-hook #'vertico-repeat-save)

(require 'orderless)
(setq completion-styles '(orderless basic)
      completion-category-overrides '((file (styles basic partial-completion))))
(setq orderless-component-separator 'orderless-escapable-split-on-space)
(setq completion-ignore-case t)
(setq orderless-smart-case t)

(require 'marginalia)
(marginalia-mode 1)

(require 'consult)
(require 'consult-imenu)
(require 'consult-flymake)
(setq completion-in-region-function 'consult-completion-in-region)

(global-set-key (kbd "C-s") 'consult-line)
(global-set-key (kbd "C-r") 'vertico-repeat)
(global-set-key (kbd "C-x g") 'consult-ripgrep)
(global-set-key (kbd "C-x C-b") 'consult-buffer)
(global-set-key (kbd "C-x C-r") 'consult-recent-file)
(global-set-key (kbd "M-g i") 'consult-imenu)
(global-set-key (kbd "M-g g") 'consult-goto-line)
(global-set-key (kbd "M-g M-g") 'consult-goto-line)
