#!/bin/bash

cd "$(dirname "$0")"
find . -name .git -type f -printf '%h\n' | sed 's/^\.//' | sort > .ignore
