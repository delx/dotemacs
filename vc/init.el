;;; -*- lexical-binding: t -*-

(dolist (pkg '("~/.emacs.d/vc/magit/lisp"
               "~/.emacs.d/vc/magit-popup"
               "~/.emacs.d/vc/magit-transient/lisp"
               "~/.emacs.d/vc/with-editor/lisp"
               "~/.emacs.d/vc/diff-hl"))
  (add-to-list 'load-path pkg))

(require 'diff-hl)
(require 'diff-hl-show-hunk)
(global-diff-hl-mode 1)

(require 'magit)
(setq transient-history-file "~/.cache/emacs/transient-history.el")
(setq magit-completing-read-function 'ivy-completing-read)
(setq git-commit-finish-query-functions
      (delete #'git-commit-check-style-conventions git-commit-finish-query-functions))
(global-set-key (kbd "C-x v s") 'magit-status)
(global-set-key (kbd "C-x v v") 'magit-dispatch)
(global-set-key (kbd "C-x v f") 'magit-file-dispatch)
